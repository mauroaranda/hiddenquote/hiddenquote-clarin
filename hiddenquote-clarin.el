;;; hiddenquote-clarin.el --- Retrieve hidden quote puzzles from Clarín -*- lexical-binding: t -*-

;; Copyright (C) 2020-2025 by Mauro Aranda

;; Author: Mauro Aranda <maurooaranda@gmail.com>
;; Maintainer: Mauro Aranda <maurooaranda@gmail.com>
;; Created: Sun Dec 13 11:10:00 2020
;; Version: 0.1
;; Package-Version: 0.1
;; Package-Requires: ((emacs "25.1") (hiddenquote "0.1"))
;; URL: http://www.mauroaranda.com
;; Keywords: games

;; This file is NOT part of GNU Emacs.

;; hiddenquote-clarin is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; hiddenquote-clarin is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with hiddenquote-clarin. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; This file defines a function, `hiddenquote-clarin-get-puzzle', to
;; retrieve hidden quote puzzles (in spanish) from Clarín, an Argentina
;; national newspaper.

;;;; Usage:
;; (require 'hiddenquote-clarin)
;; Add an entry to `hiddenquote-sources', like this:
;; (clarin html hiddenquote-get-clarin-puzzle)
;; M-x hiddenquote RET
;; Choose the clarin option.
;; With a prefix argument to `hiddenquote', you can select a specific
;; number for the puzzle, which has to be greater than
;; `hiddenquote-clarin-oldest-puzzle-available'.
;;
;; TODO:
;; - See the TODO entry in `hiddenquote-clarin-get-puzzle'.

;;; Code:

;; Requirements.
(require 'hiddenquote)
(require 'dom)
(require 'json)

(defvar hiddenquote-directory) ; In hiddenquote.el

;; Variables.
(defvar hiddenquote-clarin-oldest-puzzle-available 15033
  "The ID of the oldest hidden quote puzzle available from Clarín.")

(defvar hiddenquote-clarin-url "http://www.clarin.com/claringrilla/"
  "Base URL for retrieving hidden quote puzzle from Clarín.")

(defvar hiddenquote-clarin-solutions-regexp "privateId\":\"\\([^\"]*\\)"
  "Regexp used to find where Clarín starts giving the solutions.")

(defvar hiddenquote-clarin-end-solutions-regexp "}],\\\"syllables"
  "Regexp used to find where solutions ends.")

(defvar hiddenquote-clarin-unavailable-puzzles
  '(15096 15098 15128 15130 15148)
  "List of puzzle numbers that for some reason are unavailable.")

;; Functions.
;;;###autoload
(defun hiddenquote-clarin-get-puzzle (&optional n)
  "Get a hidden quote puzzle from Clarín.  With N nil, get today's puzzle.

With N non-nil, read a number and get that puzzle.

Clarín doesn't make available all of their puzzles.  The value of
`hiddenquote-clarin-oldest-puzzle-available' is the oldest one that can be
retrieved.  If the requested puzzle number is lower than that value,
signals an `user-error'."
  (let* ((id (and n (let ((num (read-number "Puzzle number: ")))
                      (cond ((= num 1) num)
                            ((< num hiddenquote-clarin-oldest-puzzle-available)
                             (user-error
                              "Oldest puzzle available from Clarin is %d"
                              hiddenquote-clarin-oldest-puzzle-available))
                            ((memq num hiddenquote-clarin-unavailable-puzzles)
                             (error
                              "Puzzle number %d is unavailable from Clarín"
                              num))
                            (t num)))))
         (file (and id
                    (hiddenquote-expand-puzzle-file-name
                     (format "%s.ipuz" id) "clarin"))))
    (if (and file (file-exists-p file))
        (let ((puzzle (hiddenquote-puzzle-from-ipuz
                       (with-current-buffer (find-file-noselect file t)
                         (prog1 (buffer-string)
                           (kill-buffer))))))
          (oset puzzle file file)
          puzzle)
      (let* ((url (concat hiddenquote-clarin-url
                          (if id (format "%05d" id) "")))
             (buffer (url-retrieve-synchronously url))
             dom puzzle)
        (with-current-buffer buffer
          ;; Parse HTML response from Clarín.
          (unless (setq dom (libxml-parse-html-region (point-min) (point-max)))
            (if (and (fboundp 'libxml-available-p) (libxml-available-p))
                (error "Failed to get the hiddenquote from Clarín")
              (error "Support for libxml required")))
          ;; TODO: It would be better if for every slot in
          ;; `hiddenquote-hidden-quote-puzzle' we had something that says
          ;; how to get that information from the dom, and something that says
          ;; how to filter/tweak it: it would make this code more robust.
          (let* (;; Stuff that we can get by parsing the DOM.
                 (defs (vconcat (mapcar (lambda (def)
                                          ;; Fix strange output from Clarín.
                                          (replace-regexp-in-string
                                           (concat "\\." (string 13))
                                           ""
                                           (nth 2 def)))
                                        ;; Used to be "definition-row"
                                        (dom-by-class dom "definitions-text"))))
                 ;; Strange output from Clarín.  Try to fix it.
                 (desc
                  (dom-text (last (dom-by-class dom "syllables-instruction"))))
                 (syllables (vconcat
                             (mapcar (lambda (span) (string-trim (nth 2 span)))
                                     (dom-by-class dom
                                                   "syllables-content-text"))))
                 ;; The class name dest[2-9] holds the position of the arrow,
                 ;; so get that suffix number.
                 (arrows (let* ((rows (dom-by-class dom "grid-row"))
                                (row (car rows))
                                done pos ret)
                           (while (and (not done) row)
                             (if (=
                                  (length
                                   (setq
                                    pos
                                    (seq-positions
                                     (dom-by-tag row 'input)
                                     nil
                                     (lambda (el _)
                                       (let* ((data-col (dom-attr el 'data-column-phrase)))
                                          (string= "true" data-col))))))
                                  2)
                                 (progn
                                   (setq done t
                                         ret (format "%d,%d" (1+ (car pos))
                                                     (1+ (cadr pos)))))
                               (setq row (cdr rows))))
                           ret))
                 (lengths (vconcat (mapcar (lambda (row)
                                             (length
                                              (dom-by-class row
                                                            "input-letter")))
                                           (dom-by-class dom "grid-row"))))
                 (num (car (last (car (dom-by-class dom "header-number")))))
                 (date (dom-text (dom-by-class dom "hader-date")))
                 (solutions
                  ;; Changed again on 27/02/2025.
                  (progn
                    (goto-char (point-min))
                    (let* ((private-id (progn
                                        (re-search-forward
                                         hiddenquote-clarin-solutions-regexp)
                                        (match-string 1)))
                           (url-mime-accept-string "application/json")
                           (buf (url-retrieve-synchronously
                                 (concat "https://www.clarin.com/api/gridGame/detail/" private-id)))
                           json)
                      (with-current-buffer buf
                        (goto-char (point-min))
                        (re-search-forward "^{")
                        (backward-char)
                        (setq json (json-read-from-string
                                    (decode-coding-string
                                     (buffer-substring (point) (point-max))
                                     'utf-8)))
                        (seq-map (lambda (word)
                                   (alist-get 'word word))
                                 (alist-get 'items (alist-get 'data json)))))))
                 (clues
                  (vconcat
                         (cl-mapcar (lambda (w sol)
                                      (list sol w))
                                    defs solutions))))
            ;; Fix defs, lengths and syllables, since they are duplicated.
            (setq defs (seq-take defs (/ (length defs) 2)))
            (setq lengths (seq-take lengths (/ (length lengths) 2)))
            (setq syllables (seq-take syllables (/ (length syllables) 2)))
            (setq puzzle (make-instance
                          'hiddenquote-hidden-quote-puzzle
                          :id (string-to-number num)
                          :publisher "Clarin"
                          :title (format "Claringrilla Nº%s" num)
                          :url (concat hiddenquote-clarin-url num)
                          :description desc
                          ;; More like published date, but whatever.
                          :created-date date
                          :clues clues
                          :arrows arrows
                          :field-lengths lengths
                          :syllables syllables
                          :subject "general"))
            (oset puzzle file (hiddenquote-expand-puzzle-file-name
                               (format "%s.ipuz" num) "clarin"))
            (oset puzzle saved (hiddenquote-load-saved-puzzle puzzle))))
        (kill-buffer buffer)
        puzzle))))

(provide 'hiddenquote-clarin)
;;; hiddenquote-clarin.el ends here
